﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yisoft.Framework.Utilities;

namespace Yisoft.AspNetCore.Mvc.UnitTests
{
	[TestClass]
	public class RandomUtilsUnitTests
	{
		[TestMethod]
		public void GenerateIdTest()
		{
			var result = RandomUtils.GenerateId(3);

			Console.WriteLine(result);

			Assert.IsTrue(result > 0);
		}
	}
}

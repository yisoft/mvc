﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Yisoft.AspNetCore.Mvc.Internal;

namespace Yisoft.AspNetCore.Mvc
{
	public class HttpResponseMessageResult : ActionResult
	{
		public HttpResponseMessageResult(HttpResponseMessage message,
			bool hideLocationHeader = false,
			bool withStatusCode = true,
			List<string> ignoredHeaders = null)
		{
			Message = message ?? throw new ArgumentNullException(nameof(message));
			HideLocationHeader = hideLocationHeader;
			WithStatusCode = withStatusCode;
			IgnoredHeaders = ignoredHeaders;
		}

		public HttpResponseMessage Message { get; }

		public bool HideLocationHeader { get; }

		public bool WithStatusCode { get; }

		public List<string> IgnoredHeaders { get; }

		public override Task ExecuteResultAsync(ActionContext context)
		{
			if (context == null) throw new ArgumentNullException(nameof(context));

			var executor = context.HttpContext.RequestServices.GetRequiredService<HttpResponseMessageResultExecutor>();

			return executor.ExecuteAsync(context, this);
		}
	}
}

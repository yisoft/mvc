﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Yisoft.Framework.Utilities;

namespace Yisoft.AspNetCore.Mvc.DataAnnotations
{
	public class PhoneNumberAttribute : FormValidationAttribute
	{
		public PhoneNumberAttribute() : base("PhoneNumber") { }

		protected override string AttributeName => "phone-number";

		public override bool IsValid(object value) { return ValidationUtils.IsValidPhoneNumber(value as string); }
	}
}

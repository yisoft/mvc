﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace Yisoft.AspNetCore.Mvc.DataAnnotations
{
	public class LengthAttribute : FormValidationAttribute
	{
		public LengthAttribute(int requiredLength) : base("Length") { RequiredLength = requiredLength; }

		public int RequiredLength { get; }

		protected override string AttributeName => "length";

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			var input = value as string;

			if (input?.Length == RequiredLength) return ValidationResult.Success;

			var message = GetErrorMessage(validationContext, RequiredLength);

			return new ValidationResult(message, new[] {ErrorMessageResourceName});
		}
	}
}

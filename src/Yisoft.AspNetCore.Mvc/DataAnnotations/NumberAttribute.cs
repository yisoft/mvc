﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Text.RegularExpressions;

namespace Yisoft.AspNetCore.Mvc.DataAnnotations
{
	public class NumberAttribute : FormValidationAttribute
	{
		private readonly Regex _regex = new Regex(@"^\-?\d*(\.\d+)?$", RegexOptions.Singleline | RegexOptions.Compiled);

		public NumberAttribute() : base("Number") { }

		protected override string AttributeName => "number";

		public override bool IsValid(object value) { return _regex.IsMatch(value as string); }
	}
}

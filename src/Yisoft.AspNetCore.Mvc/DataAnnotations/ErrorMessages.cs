﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

namespace Yisoft.AspNetCore.Mvc.DataAnnotations
{
	public static class ErrorMessages
	{
		public const string Checked = "Checked";
		public const string Contain = "Contain";
		public const string ContainExactly = "ContainExactly";
		public const string CreditCard = "CreditCard";
		public const string Decimal = "Decimal";
		public const string Different = "Different";
		public const string DoesntContain = "DoesntContain";
		public const string DoesntContainExactly = "DoesntContainExactly";
		public const string Email = "Email";
		public const string Empty = "Empty";
		public const string ExactCount = "ExactCount";
		public const string Integer = "Integer";
		public const string Is = "Is";
		public const string IsExactly = "IsExactly";
		public const string Length = "Length";
		public const string Match = "Match";
		public const string MaxCount = "MaxCount";
		public const string MaxLength = "MaxLength";
		public const string MinCount = "MinCount";
		public const string MinLength = "MinLength";
		public const string Not = "Not";
		public const string NotExactly = "NotExactly";
		public const string Number = "Number";
		public const string RegExp = "RegExp";
		public const string Url = "Url";
		public const string PhoneNumber = "PhoneNumber";
		public const string IdentityCard = "IdentityCard";
		public const string Test = "Test";
	}
}

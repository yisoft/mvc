﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Yisoft.AspNetCore.Mvc.Internal;

namespace Yisoft.AspNetCore.Mvc.DataAnnotations
{
	public abstract class FormValidationAttribute : ValidationAttribute, IClientModelValidator
	{
		private bool _checkedForLocalizer;
		private IStringLocalizer _stringLocalizer;

		protected FormValidationAttribute(string ruleName) : this(ruleName, typeof(DataAnnotationResources)) { }

		protected FormValidationAttribute(string ruleName, Type resourceType)
		{
			RuleName = ruleName;
			ErrorMessageResourceType = resourceType;
			ErrorMessageResourceName = ruleName;
		}

		protected string RuleName { get; }

		protected abstract string AttributeName { get; }

		public virtual void AddValidation(ClientModelValidationContext context)
		{
			if (context == null) throw new ArgumentNullException(nameof(context));

			MergeAttribute(context.Attributes, "data-val", "true");

			CheckForLocalizer(context);

			var errorMessage = GetErrorMessage(context.ModelMetadata.GetDisplayName());

			MergeAttribute(context.Attributes, "data-val-" + AttributeName.ToLower(), errorMessage);
		}

		protected static bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
		{
			if (attributes.ContainsKey(key)) return false;

			attributes.Add(key, value);

			return true;
		}

		protected string GetErrorMessage(ValidationContext validationContext, params object[] args)
		{
			var items = new List<object>(args);

			items.Insert(0, validationContext.DisplayName);

			return string.Format(ErrorMessageString, items.ToArray());
		}

		protected string GetErrorMessage(string displayName)
		{
			if (_stringLocalizer == null
			    || string.IsNullOrEmpty(ErrorMessage)
			    || !string.IsNullOrEmpty(ErrorMessageResourceName)
			    || ErrorMessageResourceType != null) return FormatErrorMessage(displayName);

			return _stringLocalizer[ErrorMessage, displayName];
		}

		protected void CheckForLocalizer(ClientModelValidationContext context)
		{
			if (_checkedForLocalizer) return;

			_checkedForLocalizer = true;

			var services = context.ActionContext.HttpContext.RequestServices;
			var options = services.GetRequiredService<IOptions<MvcDataAnnotationsLocalizationOptions>>();
			var provider = options.Value.DataAnnotationLocalizerProvider;
			var factory = services.GetService<IStringLocalizerFactory>();

			if (factory == null || provider == null) return;

			_stringLocalizer = provider(context.ModelMetadata.ContainerType ?? context.ModelMetadata.ModelType, factory);
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Microsoft.AspNetCore.Http;

// ReSharper disable once CheckNamespace
namespace Microsoft.AspNetCore.Builder
{
    public static partial class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseIf(
            this IApplicationBuilder application,
            bool condition,
            Func<IApplicationBuilder, IApplicationBuilder> action)
        {
            if (application == null) throw new ArgumentNullException(nameof(application));
            if (action == null) throw new ArgumentNullException(nameof(action));

            if (condition) application = action(application);

            return application;
        }

        public static IApplicationBuilder UseIfElse(
            this IApplicationBuilder application,
            bool condition,
            Func<IApplicationBuilder, IApplicationBuilder> ifAction,
            Func<IApplicationBuilder, IApplicationBuilder> elseAction)
        {
            if (application == null) throw new ArgumentNullException(nameof(application));
            if (ifAction == null) throw new ArgumentNullException(nameof(ifAction));
            if (elseAction == null) throw new ArgumentNullException(nameof(elseAction));

            application = condition ? ifAction(application) : elseAction(application);

            return application;
        }

        public static IApplicationBuilder UseIf(
            this IApplicationBuilder application,
            Func<HttpContext, bool> condition,
            Func<IApplicationBuilder, IApplicationBuilder> action)
        {
            if (application == null) throw new ArgumentNullException(nameof(application));
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            if (action == null) throw new ArgumentNullException(nameof(action));

            var builder = application.New();

            action(builder);

            return application.Use(next =>
            {
                builder.Run(next);

                var branch = builder.Build();

                return context => condition(context) ? branch(context) : next(context);
            });
        }

        public static IApplicationBuilder UseIfElse(
            this IApplicationBuilder application,
            Func<HttpContext, bool> condition,
            Func<IApplicationBuilder, IApplicationBuilder> ifAction,
            Func<IApplicationBuilder, IApplicationBuilder> elseAction)
        {
            if (application == null) throw new ArgumentNullException(nameof(application));
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            if (ifAction == null) throw new ArgumentNullException(nameof(ifAction));
            if (elseAction == null) throw new ArgumentNullException(nameof(elseAction));

            var ifBuilder = application.New();
            var elseBuilder = application.New();

            ifAction(ifBuilder);
            elseAction(elseBuilder);

            return application.Use(next =>
            {
                ifBuilder.Run(next);
                elseBuilder.Run(next);

                var ifBranch = ifBuilder.Build();
                var elseBranch = elseBuilder.Build();

                return context => condition(context) ? ifBranch(context) : elseBranch(context);
            });
        }
    }
}

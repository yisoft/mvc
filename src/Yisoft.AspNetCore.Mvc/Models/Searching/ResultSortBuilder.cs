﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Yisoft.Framework.Utilities;

namespace Yisoft.AspNetCore.Mvc.Models.Searching
{
	public class ResultSortBuilder<T>
	{
		private readonly List<ResultSorter> _sorters = new List<ResultSorter>();

		public IReadOnlyCollection<ResultSorter> Build() { return _sorters.AsReadOnly(); }

		public ResultSortBuilder<T> Ascending(Expression<Func<T, object>> field, int priority = 0)
		{
			_sorters.Add(new ResultSorter
			{
				FieldName = PropertyUtils.GetPropertyName(field),
				Priority = priority,
				SortType = SortTypes.Ascending
			});

			return this;
		}

		public ResultSortBuilder<T> Descending(Expression<Func<T, object>> field, int priority = 0)
		{
			_sorters.Add(new ResultSorter
			{
				FieldName = PropertyUtils.GetPropertyName(field),
				Priority = priority,
				SortType = SortTypes.Descending
			});

			return this;
		}
	}
}

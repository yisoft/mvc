﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;

namespace Yisoft.AspNetCore.Mvc.Models.Searching
{
	public class ResultSorter
	{
		public string FieldName { get; set; }

		public SortTypes SortType { get; set; }

		public int Priority { get; set; }

		public static ResultSorter Ascending(string fieldName, int priority = 0)
		{
			return new ResultSorter
			{
				FieldName = fieldName,
				Priority = priority,
				SortType = SortTypes.Ascending
			};
		}

		public static ResultSorter Descending(string fieldName, int priority = 0)
		{
			return new ResultSorter
			{
				FieldName = fieldName,
				Priority = priority,
				SortType = SortTypes.Descending
			};
		}

		internal static ResultSorter Parse(string order)
		{
			if (order == null) throw new ArgumentNullException(nameof(order));

			var field = order;
			var sortType = SortTypes.Ascending;

			if (order.StartsWith("-"))
			{
				field = order.Substring(1);
				sortType = SortTypes.Descending;
			}

			return new ResultSorter
			{
				FieldName = field,
				SortType = sortType,
				Priority = 0
			};
		}
	}
}

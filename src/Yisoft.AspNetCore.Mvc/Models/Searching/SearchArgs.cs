﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Yisoft.AspNetCore.Mvc.Models.Searching
{
    public class SearchArgs
    {
        private int _limit;
        private int _offset;

        [JsonProperty(Order = 1)]
        public int Offset
        {
            get => _offset < 0 ? 0 : _offset;
            set => _offset = value;
        }

        [JsonProperty(Order = 2)]
        public int Limit
        {
            get => _limit < 1 ? 10 : _limit;
            set => _limit = value;
        }

        [JsonProperty(Order = 4, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public IEnumerable<ResultSorter> Sorters { get; set; } = new List<ResultSorter>();
    }
}

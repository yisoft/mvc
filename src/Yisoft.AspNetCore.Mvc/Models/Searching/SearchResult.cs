﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Yisoft.AspNetCore.Mvc.Models.Searching
{
	public sealed class SearchResult : SearchResult<object>
	{
	}

	public class SearchResult<T>
	{
		public SearchResult() { }

		public SearchResult(IList<T> items, int totalCount)
		{
			TotalCount = totalCount;
			Items = items;
		}

		[JsonProperty(Order = 1)]
		public int Offset { get; set; }

		[JsonProperty(Order = 2)]
		public int Limit { get; set; }

		[JsonProperty(Order = 3)]
		public int Count => Items?.Count ?? 0;

		[JsonProperty(Order = 4)]
		public int ItemCount => Items.Count;

		[JsonProperty(Order = 5)]
		public int TotalCount { get; set; }

		[JsonProperty(Order = 6)]
		public IList<T> Items { get; set; }

		public SearchResult<T> Inject(SearchArgs searchArgs)
		{
			Offset = searchArgs.Offset;
			Limit = searchArgs.Limit;

			return this;
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Yisoft.Framework.Extensions;

namespace Yisoft.AspNetCore.Mvc.Models
{
	public class OperateResult
	{
		public static readonly OperateResult Success = new OperateResult
		{
			Succeeded = true
		};

		private readonly List<OperateError> _errors = new List<OperateError>();

		public bool Succeeded { get; protected set; }

		public IEnumerable<OperateError> Errors => _errors;

		public List<string> ErrorMessages => _errors.Select(x => x.Description).ToList();

		public List<string> ErrorCodes => _errors.Select(x => x.Code).ToList();

		public static OperateResult Failed(params OperateError[] errors)
		{
			var identityResult = new OperateResult
			{
				Succeeded = false
			};

			if (errors != null) identityResult._errors.AddRange(errors);

			return identityResult;
		}

		public override string ToString()
		{
			return Succeeded
				? "Succeeded"
				: $"Failed : {Errors.Select(x => x.Code).Join(",")}";
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.WebEncoders;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Yisoft.AspNetCore.Mvc.Configuration;
using Yisoft.AspNetCore.Mvc.Conventions;
using Yisoft.AspNetCore.Mvc.Filters;
using Yisoft.AspNetCore.Mvc.Internal;
using Yisoft.AspNetCore.Mvc.Routing;
using Yisoft.Framework.Json.Converters;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMvcExtras(this IServiceCollection services, MvcExtraSettings settings)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            if (settings == null) throw new ArgumentNullException(nameof(settings));

            //
            // configure
            //
            services.Configure<WebEncoderOptions>(options => { options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.All); });
            services.Configure<RouteOptions>(opt =>
            {
                opt.ConstraintMap.Add("email", typeof(EmailRouteConstraint));
                opt.ConstraintMap.Add("phoneNumber", typeof(PhoneNumberRouteConstraint));
            });

            //
            // mvc
            //
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(MvcExceptionFilter));

                options.Conventions.Add(new FromBodyRequiredConvention());
            });

            services.AddMvcCore()
                .AddJsonFormatters(jsonSettings =>
                {
                    jsonSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                    jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    jsonSettings.NullValueHandling = NullValueHandling.Ignore;
                    jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                    jsonSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    jsonSettings.Formatting = Formatting.Indented;
                    jsonSettings.MaxDepth = 32;

                    jsonSettings.Converters.Add(new ExtraEnumConverter {CamelCaseText = true});
                })
                .AddAuthorization();

            // localization
            if (settings.LocalizationSettings != null) services.AddLocalization(settings);

            //
            // Random Infrastructure
            //
            services.TryAddSingleton<HttpResponseMessageResultExecutor>();

            services.AddSingleton(settings.Configuration);
            services.AddSingleton<GlobalEnvironment>();

            return services;
        }
    }
}

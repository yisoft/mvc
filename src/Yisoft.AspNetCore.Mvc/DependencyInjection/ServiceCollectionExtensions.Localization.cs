﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Microsoft.Extensions.Localization;
using Yisoft.AspNetCore.Mvc.Configuration;
using Yisoft.AspNetCore.Mvc.Extensions;
using Yisoft.AspNetCore.Mvc.Internal;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLocalization(this IServiceCollection services, MvcExtraSettings settings)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            if (settings == null) throw new ArgumentNullException(nameof(settings));
            if (settings.LocalizationSettings == null) throw new ArgumentNullException(nameof(settings.LocalizationSettings));

            var localizationSettings = settings.LocalizationSettings;

            if (localizationSettings.DataAnnotationLocalizerProvider == null)
            {
                IStringLocalizer localizer = null;

                localizationSettings.DataAnnotationLocalizerProvider = (type, factory) => localizer ?? (localizer = factory.Create<DataAnnotationResources>());
            }

            services.AddLocalization();
            services.AddMvcCore()
                .AddViewLocalization(localizationSettings.Format)
                .AddDataAnnotationsLocalization(options => { options.DataAnnotationLocalizerProvider = localizationSettings.DataAnnotationLocalizerProvider; });

            return services;
        }
    }
}

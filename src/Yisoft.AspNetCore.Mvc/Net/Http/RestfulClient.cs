﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Yisoft.Framework.Net.Http;

namespace Yisoft.AspNetCore.Mvc.Net.Http
{
	public abstract class RestfulClient : RestfulClientBase
	{
		private readonly Uri _baseAddress;

		protected RestfulClient(Uri baseAddress) : base(baseAddress) { _baseAddress = baseAddress; }

		public override Uri CreateUri(string path) { return new UriBuilder(_baseAddress) {Path = path}.Uri; }

		public virtual Uri CreateUri(string path, QueryString query)
		{
			return new UriBuilder(_baseAddress)
			{
				Path = path,
				Query = query.ToUriComponent()
			}.Uri;
		}

		public virtual Uri CreateUri(QueryString query)
		{
			return new UriBuilder(_baseAddress)
			{
				Query = query.ToUriComponent()
			}.Uri;
		}

		protected override void OnFailure(HttpResponseMessage response) { }
	}
}

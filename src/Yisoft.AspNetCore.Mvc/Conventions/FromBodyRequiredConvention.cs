﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Linq;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Yisoft.AspNetCore.Mvc.Conventions
{
	public class FromBodyRequiredConvention : IActionModelConvention
	{
		public void Apply(ActionModel action)
		{
			var parameterName = action.Parameters
				.Where(p => p.BindingInfo?.BindingSource.CanAcceptDataFrom(BindingSource.Body) ?? false)
				.Select(p => p.ParameterName)
				.SingleOrDefault();

			if (parameterName != null) action.Filters.Add(new FromBodyRequiredActionFilter(parameterName));
		}

		private class FromBodyRequiredActionFilter : IActionFilter
		{
			private readonly string _parameterName;

			public FromBodyRequiredActionFilter(string parameterName) { _parameterName = parameterName; }

			public void OnActionExecuting(ActionExecutingContext context)
			{
				context.ActionArguments.TryGetValue(_parameterName, out object value);

				if (value == null)
				{
					// Now uncomment one of these depending on what you want to do.
					//
					// This will return a 400 with an error in json.
					//context.Result = new BadRequestObjectResult(new { message = "The body is required" });

					// This will add a validation error and let the action decide what to do
					//context.ModelState.AddModelError(_parameterName, "The body is required");

					context.ModelState.AddModelError(_parameterName, "The body is required");
				}
			}

			public void OnActionExecuted(ActionExecutedContext context) { }
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Net;
using System.Net.Http.Headers;

namespace Yisoft.AspNetCore.Mvc
{
	public sealed class MvcHttpException : MvcException
	{
		public MvcHttpException(object content) : this(content, null) { }

		public MvcHttpException(object content, Exception innerException) : base(nameof(MvcHttpException), innerException) { Content = content; }

		public object Content { get; }

		public HttpResponseHeaders Headers { get; set; }

		public HttpStatusCode StatusCode { get; set; }
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Yisoft.Framework.Utilities;

namespace Yisoft.AspNetCore.Mvc.Routing
{
	public sealed class EmailRouteConstraint : IRouteConstraint
	{
		public bool Match(HttpContext httpContext,
			IRouter route,
			string routeKey,
			RouteValueDictionary values,
			RouteDirection routeDirection)
		{
			if (routeDirection != RouteDirection.IncomingRequest) return false;

			return values.TryGetValue(routeKey, out object obj) && obj != null && ValidationUtils.IsValidEmailAddress(obj as string);
		}
	}
}

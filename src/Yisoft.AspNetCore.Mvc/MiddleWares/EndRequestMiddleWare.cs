﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Yisoft.AspNetCore.Mvc.MiddleWares
{
	public class EndRequestMiddleWare
	{
		private readonly RequestDelegate _next;

		public EndRequestMiddleWare(RequestDelegate next) { _next = next; }

		public async Task Invoke(HttpContext context)
		{
			context.Response.Headers.Remove("Server");

			await _next.Invoke(context);
		}
	}
}

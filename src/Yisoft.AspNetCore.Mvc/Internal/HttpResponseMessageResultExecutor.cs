﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;

namespace Yisoft.AspNetCore.Mvc.Internal
{
	public class HttpResponseMessageResultExecutor
	{
		private static readonly List<string> _IgnoredHeaders = new List<string>
		{
			HeaderNames.Date,
			HeaderNames.TransferEncoding,
			HeaderNames.Server,
			"X-SourceFiles",
			"X-Powered-By"
		};

		public async Task ExecuteAsync(ActionContext context, HttpResponseMessageResult result)
		{
			if (context == null) throw new ArgumentNullException(nameof(context));
			if (result == null) throw new ArgumentNullException(nameof(result));

			var response = context.HttpContext.Response;
			var message = result.Message;

			if (result.WithStatusCode) response.StatusCode = (int) message.StatusCode;

			var ignoredHeaders = new List<string>(_IgnoredHeaders);

			if (result.HideLocationHeader) ignoredHeaders.Add(HeaderNames.Location);
			if (result.IgnoredHeaders != null) ignoredHeaders.AddRange(result.IgnoredHeaders);

			_WriteHeaders(response, message, ignoredHeaders);

			await message.Content.ReadAsStreamAsync().ContinueWith(stream => { stream.Result.CopyTo(response.Body); });
		}

		private static void _WriteHeaders(HttpResponse response, HttpResponseMessage message, ICollection<string> ignoredHeaders)
		{
			foreach (var header in message.Headers)
			{
				if (ignoredHeaders.Contains(header.Key)) continue;

				response.Headers[header.Key] = new StringValues(header.Value.ToArray());
			}

			foreach (var header in message.Content.Headers)
			{
				if (ignoredHeaders.Contains(header.Key)) continue;

				response.Headers[header.Key] = new StringValues(header.Value.ToArray());
			}
		}
	}
}

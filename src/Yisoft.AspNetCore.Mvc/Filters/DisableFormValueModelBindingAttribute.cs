﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Yisoft.AspNetCore.Mvc.Filters
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class DisableFormValueModelBindingAttribute : Attribute, IResourceFilter
	{
		public void OnResourceExecuting(ResourceExecutingContext context)
		{
			var formValueProviderFactory = context.ValueProviderFactories
				.OfType<FormValueProviderFactory>()
				.FirstOrDefault();

			if (formValueProviderFactory != null) context.ValueProviderFactories.Remove(formValueProviderFactory);

			var jqueryFormValueProviderFactory = context.ValueProviderFactories
				.OfType<JQueryFormValueProviderFactory>()
				.FirstOrDefault();

			if (jqueryFormValueProviderFactory != null) context.ValueProviderFactories.Remove(jqueryFormValueProviderFactory);
		}

		public void OnResourceExecuted(ResourceExecutedContext context) { }
	}
}

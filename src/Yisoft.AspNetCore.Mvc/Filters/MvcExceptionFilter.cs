﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Yisoft.AspNetCore.Mvc.Filters
{
	public class MvcExceptionFilter : IExceptionFilter
	{
		public void OnException(ExceptionContext context)
		{
			var mvcException = context.Exception as MvcException;

			if (mvcException == null) return;

			_ProcessMvcException(context, mvcException);
		}

		private void _ProcessMvcException(ExceptionContext context, MvcException mvcException)
		{
			var mvcHttpException = mvcException as MvcHttpException;

			if (mvcHttpException == null) return;

			_ProcessMvcHttpException(context, mvcHttpException);
		}

		private void _ProcessMvcHttpException(ExceptionContext context, MvcHttpException mvcHttpException)
		{
			if (mvcHttpException == null) return;

			var result = new BadRequestObjectResult(mvcHttpException.Content);

			context.Result = result;
			context.ExceptionHandled = false;
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Yisoft.AspNetCore.Mvc.Configuration
{
	public class GlobalEnvironment
	{
		public GlobalEnvironment(IServiceProvider applicationServices, IHostingEnvironment hostingEnvironment, IConfigurationRoot configuration)
		{
			ApplicationServices = applicationServices;
			HostingEnvironment = hostingEnvironment;
			Configuration = configuration;
		}

		public IHostingEnvironment HostingEnvironment { get; }

		public IConfigurationRoot Configuration { get; }

		public IServiceProvider ApplicationServices { get; }
	}
}

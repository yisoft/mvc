﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

namespace Yisoft.AspNetCore.Mvc.Utils
{
	public static class MultipartRequestHelper
	{
		// Content-Type: multipart/form-data; boundary="----WebKitFormBoundarymx2fSWqWSd0OxQqq"
		// The spec says 70 characters is a reasonable limit.
		public static string GetBoundary(MediaTypeHeaderValue contentType, int lengthLimit)
		{
			var boundary = HeaderUtilities.RemoveQuotes(contentType.Boundary);

			if (string.IsNullOrWhiteSpace(boundary)) throw new InvalidDataException("Missing content-type boundary.");
			if (boundary.Length > lengthLimit) throw new InvalidDataException($"Multipart boundary length limit {lengthLimit} exceeded.");

			return boundary;
		}

		public static bool IsMultipartContentType(string contentType)
		{
			return !string.IsNullOrEmpty(contentType)
			       && contentType.IndexOf("multipart/", StringComparison.OrdinalIgnoreCase) >= 0;
		}

		public static bool HasFormDataContentDisposition(ContentDispositionHeaderValue contentDisposition)
		{
			// Content-Disposition: form-data; name="key";
			return contentDisposition != null
			       && contentDisposition.DispositionType.Equals("form-data")
			       && string.IsNullOrEmpty(contentDisposition.FileName)
			       && string.IsNullOrEmpty(contentDisposition.FileNameStar);
		}

		public static bool HasFileContentDisposition(ContentDispositionHeaderValue contentDisposition)
		{
			// Content-Disposition: form-data; name="myfile1"; filename="Misc 002.jpg"
			return contentDisposition != null
			       && contentDisposition.DispositionType.Equals("form-data")
			       && (!string.IsNullOrEmpty(contentDisposition.FileName)
			           || !string.IsNullOrEmpty(contentDisposition.FileNameStar));
		}

		public static Encoding GetEncoding(MultipartSection section)
		{
			var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out MediaTypeHeaderValue mediaType);

			// UTF-7 is insecure and should not be honored. UTF-8 will succeed for most cases.
			return hasMediaTypeHeader && !Encoding.UTF7.Equals(mediaType.Encoding) ? mediaType.Encoding : Encoding.UTF8;
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Yisoft.Framework;

namespace Yisoft.AspNetCore.Mvc.Extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder AddJsonFile(this IConfigurationBuilder builder,
            string path,
            IHostingEnvironment env,
            bool optional = true,
            bool reloadOnChange = true)
        {
            return AddJsonFile(builder, path, env.EnvironmentName, optional, reloadOnChange);
        }

        public static IConfigurationBuilder AddJsonFile(this IConfigurationBuilder builder,
            string path,
            string environmentName,
            bool optional = true,
            bool reloadOnChange = true)
        {
            var optionalPath = HostingEnvironments.Postfix(string.Empty, environmentName, HostingEnvironments.Production, ".");

            builder.AddJsonFile(path.Replace("@", "."), optional, reloadOnChange);
            builder.AddJsonFile(path.Replace("@", string.Concat(optionalPath, ".")), optional, reloadOnChange);

            return builder;
        }

        public static IConfigurationBuilder AddIf(this IConfigurationBuilder builder,
            bool condition,
            Func<IConfigurationBuilder, IConfigurationBuilder> action)
        {
            if (condition) action?.Invoke(builder);

            return builder;
        }

        public static IConfigurationBuilder AddIfElse(this IConfigurationBuilder builder,
            bool condition,
            Func<IConfigurationBuilder, IConfigurationBuilder> ifAction,
            Func<IConfigurationBuilder, IConfigurationBuilder> elseAction)
        {
            if (condition) ifAction?.Invoke(builder);
            else elseAction?.Invoke(builder);

            return builder;
        }

        public static T GetSection<T>(this IConfiguration configuration, string key = null) where T : new()
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));
            if (key == null) key = typeof(T).Name;

            var section = new T();

            configuration.GetSection(key).Bind(section);

            return section;
        }
    }
}

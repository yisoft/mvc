﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using Yisoft.Framework;

namespace Yisoft.AspNetCore.Mvc.Extensions
{
    public static class LoggerFactoryExtensions
    {
        public static ILoggerFactory AddIf(
            this ILoggerFactory loggerFactory,
            bool condition,
            Func<ILoggerFactory, ILoggerFactory> action)
        {
            if (condition) loggerFactory = action(loggerFactory);

            return loggerFactory;
        }

        public static ILoggerFactory AddIfElse(
            this ILoggerFactory loggerFactory,
            bool condition,
            Func<ILoggerFactory, ILoggerFactory> ifAction,
            Func<ILoggerFactory, ILoggerFactory> elseAction)
        {
            if (condition)
            {
                ifAction(loggerFactory);
            }
            else
            {
                elseAction(loggerFactory);
            }

            return loggerFactory;
        }

        public static ILoggerFactory AddNLogWeb(this ILoggerFactory loggerFactory, IApplicationBuilder app, IHostingEnvironment env)
        {
            var fileName = env.Postfix("nlog", HostingEnvironments.Production, ".") + ".config";
            var nlogConfig = Path.Combine(env.ContentRootPath, "compiler", "config", fileName);

            loggerFactory.AddNLogWeb(app, nlogConfig);

            return loggerFactory;
        }

        public static ILoggerFactory AddNLogWeb(this ILoggerFactory loggerFactory, IApplicationBuilder app, string configFileRelativePath)
        {
            loggerFactory.AddNLog().ConfigureNLog(configFileRelativePath);

            app.AddNLogWeb();

            return loggerFactory;
        }
    }
}

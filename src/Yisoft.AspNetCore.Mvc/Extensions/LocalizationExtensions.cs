﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;

namespace Yisoft.AspNetCore.Mvc.Extensions
{
	public static class LocalizationExtensions
	{
		public static IStringLocalizer Create<T>(this IStringLocalizerFactory localizerFactory, string prefix = null)
		{
			var args = _GetParams<T>();
			var baseName = string.IsNullOrWhiteSpace(prefix) ? args.Item1 : args.Item1.Substring(prefix.Length);

			return localizerFactory.Create(baseName, args.Item2);
		}

		public static IHtmlLocalizer Create<T>(this IHtmlLocalizerFactory localizerFactory, string prefix = null)
		{
			var args = _GetParams<T>();
			var baseName = string.IsNullOrWhiteSpace(prefix) ? args.Item1 : args.Item1.Substring(prefix.Length);

			return localizerFactory.Create(baseName, args.Item2);
		}

		private static Tuple<string, string> _GetParams<T>()
		{
			var type = typeof(T);
			var assemblyName = type.GetTypeInfo().Assembly.GetName();

			return new Tuple<string, string>(type.FullName, assemblyName.Name);
		}
	}
}

﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Yisoft.AspNetCore.Mvc.Models;

namespace Yisoft.AspNetCore.Mvc.Extensions
{
	public static class ControllerExtensions
	{
		public static string Action<T>(this Controller controller,
			string actionName,
			object values = null,
			string protocol = null,
			string host = null,
			string fragment = null)
			where T : Controller
		{
			var name = typeof(T).Name;
			var controllerName = name.EndsWith("Controller")
				? name.Substring(0, name.Length - 10)
				: name;
			return controller.Url.Action(actionName, controllerName, values, protocol, host, fragment);
		}

		public static HttpResponseMessageResult HttpResponse(this Controller controller,
			HttpResponseMessage message,
			bool hideLocationHeader = false,
			bool withStatusCode = true,
			List<string> ignoredHeaders = null)
		{
			if (message == null) throw new ArgumentNullException(nameof(message));

			return new HttpResponseMessageResult(message, hideLocationHeader, withStatusCode, ignoredHeaders);
		}

		public static void ThrowIfModelStateUnvalidated(this Controller controller)
		{
			if (controller.ModelState.IsValid) return;

			var errors = (from key in controller.ModelState.Keys
			              let item = controller.ModelState[key]
			              where item.ValidationState == ModelValidationState.Invalid && item.Errors.Count != 0
			              select new OperateError
			              {
				              Code = key,
				              Description = item.Errors.FirstOrDefault().ErrorMessage
			              }).ToList();

			throw new MvcHttpException(errors);
		}
	}
}

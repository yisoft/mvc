﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Yisoft.AspNetCore.Mvc.Models.Searching;

namespace Yisoft.AspNetCore.Mvc.Extensions
{
	public static class SearchingQueryableExtensions
	{
		public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, Action<ResultSortBuilder<T>> sortAction)
		{
			var sortBuilder = new ResultSortBuilder<T>();

			sortAction?.Invoke(sortBuilder);

			var sorters = sortBuilder.Build();

			return source.OrderBy(sorters);
		}

		public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, ResultSortBuilder<T> sortBuilder)
		{
			var sorters = sortBuilder.Build();

			return source.OrderBy(sorters);
		}

		public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, IEnumerable<ResultSorter> sorters)
		{
			if (sorters == null) return source;

			var resultSorters = sorters.ToArray();

			if (!resultSorters.Any()) return source;

			return resultSorters.Aggregate<ResultSorter, IOrderedQueryable<T>>(null, (current, sorter) => current == null
				? source.OrderBy(sorter.FieldName, sorter.SortType)
				: current.OrderBy(sorter.FieldName, sorter.SortType, false));
		}

		public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string field, SortTypes sortType, bool isFirst = true)
		{
			var param = Expression.Parameter(typeof(T), string.Empty);
			var property = Expression.PropertyOrField(param, field);
			var sort = Expression.Lambda(property, param);
			var method = $"{(isFirst ? "OrderBy" : "ThenBy")}{(sortType == SortTypes.Descending ? "Descending" : string.Empty)}";

			var call = Expression.Call(
				typeof(Queryable),
				method,
				new[] {typeof(T), property.Type},
				source.Expression,
				Expression.Quote(sort));

			return (IOrderedQueryable<T>) source.Provider.CreateQuery<T>(call);
		}
	}
}

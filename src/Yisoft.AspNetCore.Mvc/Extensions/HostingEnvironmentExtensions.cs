﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Microsoft.AspNetCore.Hosting;
using Yisoft.Framework;

namespace Yisoft.AspNetCore.Mvc.Extensions
{
	public static class HostingEnvironmentExtensions
	{
		public static bool IsLocal(this IHostingEnvironment env) { return env.IsEnvironment(HostingEnvironments.Local); }

		public static bool IsTest(this IHostingEnvironment env) { return env.IsEnvironment(HostingEnvironments.Test); }

		public static bool IsDemonstration(this IHostingEnvironment env) { return env.IsEnvironment(HostingEnvironments.Demonstration); }

		public static string NormalizeEnvName(this IHostingEnvironment env, string defaultValue = HostingEnvironments.Development)
		{
			return HostingEnvironments.NormalizeEnvName(env.EnvironmentName, defaultValue);
		}

		public static string Postfix(this IHostingEnvironment env,
			string input,
			string hiddenValue = HostingEnvironments.Production,
			string separator = "_")
		{
			var envName = env.NormalizeEnvName();

			return HostingEnvironments.Postfix(input, envName, hiddenValue, separator);
		}
	}
}

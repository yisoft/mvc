﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiDemo.Models;
using Yisoft.AspNetCore.Mvc.Models.Searching;

namespace WebApiDemo.Controllers
{
	[ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/identity/users")]
	public abstract class DataApiController<TModel, TKey> : Controller
		where TModel : class, IEntity<TKey>
		where TKey : IEquatable<TKey>
	{
		// TODO
		//[HttpGet]
		//public virtual async Task<IActionResult> Get(SearchArgs)
		//{
		//	var result = _userService.Search(model);

		//	if (result.ItemCount == 0) return NoContent();

		//	return Ok(result);
		//}
	}
}
